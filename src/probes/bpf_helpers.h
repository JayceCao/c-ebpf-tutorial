#ifndef _BPF_HELPERS_H_
#define _BPF_HELPERS_H_

#define __uint(name, val) int(*name)[val]
#define __type(name, val) typeof(val) *name
#define __array(name, val) typeof(val) *name[]

static int (*bpf_probe_read)(void *dst, u64 size, const void *unsafe_ptr) =
    (void *)BPF_FUNC_probe_read;
static void *(*bpf_map_lookup_elem)(void *map, void *key) = (void *)
    BPF_FUNC_map_lookup_elem;
static int (*bpf_trace_printk)(const char *fmt, u64 fmt_size,
                               ...) = (void *)BPF_FUNC_trace_printk;
static int (*bpf_probe_read_str)(void *dst, u64 size, const void *unsafe_ptr) =
    (void *)BPF_FUNC_probe_read_str;
static int (*bpf_probe_read_user_str)(void *dst, u64 size,
                                      const void *unsafe_ptr) = (void *)
    BPF_FUNC_probe_read_user_str;

#endif
