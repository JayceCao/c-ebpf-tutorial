#include <bpf/libbpf.h>
#include <getopt.h>
#include <iostream>
#include <unistd.h>

int main(int argc, char **argv)
{
    int opt, err_code;
    char *filename = nullptr;

    while ((opt = getopt(argc, argv, "f:")) != -1) {
        switch (opt) {
        case 'f':
            filename = optarg;
            break;

        default:
            std::cerr << "wrong usage.\n";
            return 1;
        }
    }

    struct bpf_object *obj = bpf_object__open_file(filename, nullptr);
    if ((err_code = libbpf_get_error(obj))) {
        std::cerr << "opening BPF object file " << filename
                  << " failed: " << strerror(err_code) << std::endl;
        return 1;
    }

    if (bpf_object__load(obj)) {
        std::cerr << "loading BPF object file " << filename
                  << " failed: " << strerror(errno) << std::endl;
        return 1;
    }

    struct bpf_program *prog;
    struct bpf_link *link;
    bpf_object__for_each_program(prog, obj)
    {
        link = bpf_program__attach(prog);
        if (libbpf_get_error(link)) {
            std::cerr << "bpf_program__attach failed\n";
            return 1;
        }
    }

    getchar();

    return 0;
}
