find_package(PkgConfig REQUIRED)
pkg_check_modules(LIBBPF REQUIRED libbpf)

add_subdirectory(hello_ebpf)
add_subdirectory(probes)
